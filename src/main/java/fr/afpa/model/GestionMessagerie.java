package fr.afpa.model;

import fr.afpa.beans.Message;
import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.GestionDao;

public class GestionMessagerie {
	
	/**
	 * Fonction de recherche de message par mot-clé
	 * @param la fonction prend en paramètre une chaine de caractère correspondant au mot-clé recherché
	 * et affichera tout les messages dont l'objet contient ce mot-clé.
	 */
	public  void rechercheMessage (String motCle) {
		
		GestionDao gestionRechercheMessage = new GestionDao();
		gestionRechercheMessage.rechercherMessage(motCle);
		
	}
	
	
	public void enregistrerChamps(String nom, String prenom, String mail, String tel, String login, String mdp) {
		
		Utilisateur user = new Utilisateur(login, mdp, nom,prenom,mail,tel);
		GestionDao gestionCreationUser = new GestionDao();
		gestionCreationUser.enregistrerUser(user);
		
	}
	
	
	
	/**
	 * Fonction d'affichage des messages reçus par l'utilisateur
	 * 
	 */
	public  void displayMessagesRecus () {
		
	}
	
	/**
	 * Fonction d'affichage des messages envoyés par l'utilisateur
	 * 
	 */
	public  void displayMessagesEnvoyes () {
		
	}
	
	/**
	 * Fonction d'affichage du nombre de messages reçus par l'utilisateur
	 * 
	 */
	public static void displayNbMessagesRecus () {
		
	}
	
	/**
	 * Fonction d'affichage du nombre de messages envoyés par l'utilisateur
	 * 
	 */
	public static void displayNbMessagesEnvoyes () {
		
	}
	
	/**
	 * Fonction permettant d'envoyer un message à un utilisateur
	 * @param Elle prend en parametre un objet de type Message
	 */
	public void envoyerMessage (String objet, String contenu, String destinataire) {
		Message message = new Message(objet, contenu, destinataire);
		GestionDao gestionEnvoiMessage = new GestionDao();
		gestionEnvoiMessage.envoyerMessage(message);
	}

}
