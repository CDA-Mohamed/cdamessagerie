package fr.afpa.beans;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor

@Entity

@NamedQueries({
    
   @NamedQuery(name = "rechercheMessage", query = "FROM Utilisateur u, Message m where position(:motcle in m.objet) > 0")
     
})
public class Message {
	

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idMessage;
	private String objet = "Bienvenue";
	private String contenu = "Nous vous souhaitons la bienvenue parmi nous !";
	private boolean lu;
	private Date date;
	private String destinataire;
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="idUtilisateur")
	private Utilisateur user;


public Message(String objet) {
	this.objet = objet;
}


public Message(String objet, String contenu, String destinataire) {
	this.objet = objet;
	this.contenu = contenu;
	this.destinataire = destinataire;
	
}


@Override
public String toString() {
	return objet + "        " + contenu;
}

}