package fr.afpa.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor

@Entity

public class Utilisateur {
	

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY)
	private int idUtilisateur;
	@Column(unique=true)
	private String login;
	private String mdp;
	private String nom;
	private String prenom;
	@Column(unique=true)
	private String email;
	private String numTel;
	@OneToMany(mappedBy ="user")
	private List<Message> listeMessages;
	
	public Utilisateur(String login, String mdp) {
	login = this.login;
	mdp = this.mdp;
	}

	public Utilisateur(String login, String mdp, String nom, String prenom, String mail, String tel) {
		this.login = login;
		this.mdp = mdp;
		this.nom = nom;
		this.prenom = prenom;
		this.email = mail;
		this.numTel = tel;
	}

	@Override
	public String toString() {
		return "Utilisateur [login=" + login + ", mdp=" + mdp + ", nom=" + nom + ", prenom=" + prenom + ", email="
				+ email + ", numTel=" + numTel + "]";
	}
	
}
