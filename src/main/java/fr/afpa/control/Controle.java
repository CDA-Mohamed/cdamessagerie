package fr.afpa.control;

import fr.afpa.beans.Utilisateur;
import fr.afpa.dao.GestionDao;
import fr.afpa.model.GestionMessagerie;
import fr.afpa.view.ErreurConnexion;
import fr.afpa.view.Menumessagerie;

public class Controle {

	/**
	 * Fonction de vérification de la saisie d'email
	 * @param email : la fonction prend en parametre l'email à verifier
	 * @return Elle renvoie true si le mail a déjà été enregistré et false dans le cas contraire
	 */
	
	public boolean checkChampMail (String email) {
		if (email != "") {
			return true;
		}
		return false;
	}
	
	/**
	 * Fonction de vérification de la saisie du login
	 * @param email : la fonction prend en parametre le login à verifier
	 * @return Elle renvoie true si le login a déjà été enregistré et false dans le cas contraire
	 */
	
	public boolean checkChampLogin (String login) {
		if (login != "") {
			return true;
		}
		return false;
	}
	
	/**
	 * Fonction de vérification du champ de recherche
	 * @param champ : la fonction prend en parametre le champ de texte à rechercher
	 * @return Elle renvoie true si le champ n'est pas vide et false dans le cas contraire
	 */
	
	public boolean checkChampRecherche (String champ) {
		if (champ != "") {
			return true;
		}
		return false;
	}
	
	/**
	 * Fonction de vérification du compte utilisateur
	 * @param email : la fonction prend en parametre un login et un mot de passe
	 * @return Elle renvoie true si le compte est enregistré  en BDD et false dans le cas contraire
	 */
	
	public static boolean checkAuth (String login, String mdp) {
		
		return false;
	}
	
	/**
	 * Fonction de vérification des champs de saisie d'authentification
	 * @param email : la fonction prend en parametre 2 chaines de caracteres correspondants aux champs de connexion
	 * @return Elle renvoie true si les champs sont vides et false dans le cas contraire
	 */
	
	public static boolean checkField (String field1, String field2) {
		
		return false;
	}
	
	
	public boolean checkChamps (String nom, String prenom, String mail, String tel, String login, String mdp) {
		if(nom != "" && prenom != "" && mail != "" && tel != "" && login != "" && mdp != "") {
			
			return true;
		}
		return false;
	}

	public boolean checkChampEnvoi(String destinataire, String objet, String contenu) {
		if(destinataire != "" && objet != "" && contenu != "") {
			return true;
		}
		return false;
	}
	
	public boolean checkChampLogin (String login, String motDePasse) {
		//System.out.println("§§§§§§§§§  checkChampLogin");
		
		if (login != "" && motDePasse != "") {
			
			GestionDao gd = new GestionDao();
			Utilisateur user = new Utilisateur();
			user.setLogin(login);
			user.setMdp(motDePasse);
			user = gd.checkAuthentification(user);
			
			if(user != null) {
				Menumessagerie menuPrincipal = new Menumessagerie();
				
        menuPrincipal.activerFrame();
        System.out.println("la liste n'est pas nulle");
        return true;
			}
			 else if(user == null) {
    	 ErreurConnexion erreur = new ErreurConnexion();
   	 erreur.activerFrame();
   	 System.out.println("la liste est nulle");
   	 return false;
     }
			
			
		
		}
		return false;
	}
}
