package fr.afpa.view;


	
	

	import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
	import java.awt.Font;
	import java.awt.GridLayout;
	import java.awt.Image;
	import java.awt.Toolkit;
	import java.awt.event.ActionEvent;
	import java.awt.event.ActionListener;
	import java.awt.event.InputMethodEvent;
	import java.awt.event.InputMethodListener;
	import java.awt.event.WindowEvent;
	import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
	import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
	import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
	import javax.swing.JTextArea;
	import javax.swing.JTextField;
	import javax.swing.border.Border;

import fr.afpa.beans.Message;
import fr.afpa.control.Controle;
import fr.afpa.dao.GestionDao;
import fr.afpa.model.GestionMessagerie;


	public class MenuRechercheMessage extends JFrame implements ActionListener, InputMethodListener, WindowListener {	
		
		
		private JLabel videGauche = new JLabel("          ");
		private JLabel videDroit = new JLabel("                                    ");
		private JPanel espaceGauche = new JPanel();
		private JPanel espaceDroit = new JPanel();
		private JPanel espaceBas = new JPanel();
		private JPanel panelNord = new JPanel();
		private JPanel panelCentre = new JPanel();
		private JPanel panelMessage = new JPanel();
		JList<Message> listeMessages = new JList<>();
		DefaultListModel<Message> model = new DefaultListModel();
		private JLabel bienvenue = new JLabel("Bonjour Nom Prénom");
		private JTextField champsRecherche = new JTextField();
		private JButton rechercher = new JButton("Rechercher >");
		private JMenuBar menu = new JMenuBar();
		private JMenu fichier = new JMenu("Fichier");
		private JMenu edit = new JMenu("Édition");
		private JMenu help = new JMenu("Help");
		private JMenuItem fich = new JMenuItem("Nouveau message");
		private JMenuItem deco = new JMenuItem("Deconnexion");
		private JMenuItem edi1 = new JMenuItem("Visualiser compte");
		private JMenuItem edi2 = new JMenuItem("Modifier compte");
		private JMenuItem hel = new JMenuItem();
		
		private Border cadre = BorderFactory.createTitledBorder("Messages trouvés :");
		private JTextArea message = new JTextArea();
		
		
		public MenuRechercheMessage() {
			
			// Paramètres de la frame principale
			
			this.setTitle("CDA Messagerie");
			BorderLayout borderFrame = new BorderLayout();
			this.setLayout(borderFrame);
			this.setBounds(600,200, 700, 600);
			this.setResizable(false);		
			this.addWindowListener(this);
			
			// Ajout d'une icone personnalisée
			
					Toolkit kit = Toolkit.getDefaultToolkit();		
					Image img = kit.getImage("C:/ENV/messenger.png");
					setIconImage(img);
					
			// Ajout de la barre de menu
					
					fichier.add(fich);
					fichier.add(deco);
					edit.add(edi1);
					edit.add(edi2);
					help.add(hel);
					menu.add(fichier); 
					menu.add(edit);
					menu.add(help);
					this.setJMenuBar(menu);
					
					
			FlowLayout fenetreTitre = new FlowLayout(FlowLayout.LEFT);
			panelNord.setLayout(fenetreTitre);
			Font font = new Font("Verdana", Font.BOLD, 20);
			Font font2 = new Font ("Verdana",Font.BOLD, 14);
			bienvenue.setFont(font);
			panelNord.add(bienvenue);
			this.add(panelNord,BorderLayout.NORTH);
			
			FlowLayout grilleMessage = new FlowLayout(FlowLayout.LEFT);
			panelCentre.setLayout(grilleMessage);
			rechercher.setFont(font2);
			ImageIcon imgRechercher = new ImageIcon("C:/ENV/loupe.png");
			rechercher.setIcon(imgRechercher);
			Dimension dim = new Dimension(150,35);
			champsRecherche.setPreferredSize(dim);

			panelCentre.add(champsRecherche);
			panelCentre.add(rechercher);
			
			
		
			this.add(panelCentre, BorderLayout.CENTER);
			
			BorderLayout fenetreSud = new BorderLayout();
			panelMessage.setBorder(cadre);
			panelMessage.setLayout(fenetreSud);
			
			// Instanciation d'une gestionDao pour récuperer la liste de message et l'afficher dans la JList
			
			
			
			message.setSize(500, 400);
			message.setPreferredSize(message.getSize());
			panelMessage.add(listeMessages,BorderLayout.NORTH);
			panelMessage.add(espaceBas,BorderLayout.CENTER);
			
			this.add(panelMessage,BorderLayout.SOUTH);
			
			FlowLayout flowW = new FlowLayout();
			espaceGauche.setLayout(flowW);
			espaceGauche.add(videGauche);
			this.add(espaceGauche,BorderLayout.WEST);
			
			FlowLayout flowE = new FlowLayout();
			espaceDroit.setLayout(flowE);
			espaceDroit.add(videDroit);
			this.add(espaceDroit,BorderLayout.EAST);
			message.setEditable(false);
			
			
			rechercher.addActionListener(this);
			champsRecherche.addInputMethodListener(this);
			
		}

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosed(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosing(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void caretPositionChanged(InputMethodEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void inputMethodTextChanged(InputMethodEvent arg0) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			Controle controleChampRecherche = new Controle();
			if (controleChampRecherche.checkChampRecherche(champsRecherche.getText())) {
				GestionMessagerie gestionChampRecherche = new GestionMessagerie();
				gestionChampRecherche.rechercheMessage(champsRecherche.getText());
				
				GestionDao gestion = new GestionDao();
				listeMessages.setModel(model);
				List <Message> listeRechercheMessages = gestion.rechercherMessage(champsRecherche.getText());
				for (int i = 0; i < listeRechercheMessages.size(); i++)
				model.addElement(listeRechercheMessages.get(i));
				this.desactiverFrame();
				
				
			}
			
		}
		
		public void activerFrame() {
			this.setVisible(true);
		}
		
		public void desactiverFrame() {
			this.setVisible(false);
		}
		
		

	}



