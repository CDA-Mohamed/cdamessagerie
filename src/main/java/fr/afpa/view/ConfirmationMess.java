package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Label;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;

public class ConfirmationMess extends JFrame implements ActionListener {
	
	
	JLabel compte = new JLabel("                                  ");
	Panel p = new Panel(new FlowLayout(FlowLayout.LEFT));
	JLabel confirm = new JLabel("Message envoyé");
	JButton b = new JButton("Retour à la page d'accueil >");
	JPanel pcenter = new JPanel(new FlowLayout(FlowLayout.CENTER));
	JPanel pnorth = new JPanel();
	JPanel peast = new JPanel();
	JPanel pwest = new JPanel();
	JPanel pc = new JPanel();
	Border blackline = BorderFactory.createTitledBorder(" Création de compte");
	JPanel psouth = new JPanel();
	private Label t1 = new Label("                                 ");
	private Label t3 = new Label("                                 ");
	private Label t2 = new Label("             ");

	public ConfirmationMess() {
		super();
	
		// Ajout d'une icone personnalisée
		
		Toolkit kit = Toolkit.getDefaultToolkit();		
		Image img = kit.getImage("C:/ENV/messenger.png");
		setIconImage(img);
		
		pnorth.setLayout(new BorderLayout());
		this.setTitle("CDA Messagerie");
		this.setBounds(600, 200, 700, 200);
		p.add(compte);
		pc.add(confirm);
		pnorth.add(p,BorderLayout.WEST);
		pc.add(b);
		this.add(pnorth,BorderLayout.NORTH);
		pcenter.add(pc);
		//pcenter.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black));
		pcenter.setBorder(blackline);
		this.add(pcenter,BorderLayout.CENTER);
		
		pwest.add(t3);
		psouth.add(t1);
		peast.add(t2);
		this.add(peast,BorderLayout.EAST);
		this.add(pwest,BorderLayout.WEST);
		this.add(psouth,BorderLayout.SOUTH);
		b.addActionListener(this);
	}
	
	public void activerFrame() {
    	this.setVisible(true);
	}
	
	public void desactiverFrame() {
    	this.setVisible(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
	Connexion nm = new Connexion();
	nm.activerFrame(); 
	this.desactiverFrame();
		
	}
}
