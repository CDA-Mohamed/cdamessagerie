package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;

import fr.afpa.control.Controle;
import fr.afpa.model.GestionMessagerie;

public class MenuCreationCompte extends JFrame implements ActionListener, InputMethodListener, WindowListener {

	
	
	private JPanel menu = new JPanel();
	private JPanel champs = new JPanel();
	private JPanel boutonVal = new JPanel();
	private JPanel espaceE = new JPanel();
	private JPanel espaceW = new JPanel();
	private JLabel espaceVide = new JLabel        ("                                  ");
	private JLabel espaceVide1 = new JLabel        ("                                 ");
	private JLabel espaceVide2 = new JLabel        ("                                 ");
	private Border titre = BorderFactory.createTitledBorder(" Création de compte : ");
	private JLabel nom = new JLabel("     Nom : ");
	private JLabel prenom = new JLabel("     Prénom : ");
	private JLabel mail = new JLabel("     Mail : ");
	private JLabel numTel = new JLabel("     Numéro de téléphone : ");
	private JLabel login = new JLabel("     Login : ");
	private JLabel mdp = new JLabel("    Mot de passe : ");
	private JTextField tNom = new JTextField("",30);
	private JTextField tPrenom = new JTextField();
	private JTextField tMail = new JTextField();
	private JTextField tNumTel = new JTextField();
	private JTextField tLogin = new JTextField();
	private JTextField tMdp = new JTextField();
	private JButton valider = new JButton("Valider");
	
	
	public MenuCreationCompte () {
		super();
		
		// Parametrage de la fenetre principale du menu de creation de compte
		
		this.setTitle("CDA Messagerie");
		BorderLayout borderFrame = new BorderLayout();
		this.setLayout(borderFrame);
		this.setBounds(600,200, 700, 350);
		
		//this.setResizable(false);
		this.setVisible(true);		
		this.addWindowListener(this);
		
		// Ajout d'une icone personnalisée
		
		Toolkit kit = Toolkit.getDefaultToolkit();		
		Image img = kit.getImage("C:/ENV/messenger.png");
		setIconImage(img);
		
		
		// Ajout d'un panel contenant le titre de la fenetre 
		
		FlowLayout fenetreTitre = new FlowLayout(FlowLayout.LEFT);
		menu.setLayout(fenetreTitre);
		this.add(menu,BorderLayout.NORTH);
		
		
		// Ajout des labels et des champs
		
		GridLayout fenetreChamps = new GridLayout(6,3);
		champs.setLayout(fenetreChamps);
		
		champs.add(nom);
		champs.add(tNom);
		champs.add(Box.createGlue());
	
		
		
		champs.add(prenom);
		champs.add(tPrenom);
		champs.add(Box.createGlue());
	
		
		
		champs.add(mail);
		champs.add(tMail);
		champs.add(Box.createGlue());
		
		
		
		champs.add(numTel);
		champs.add(tNumTel);
		champs.add(Box.createGlue());

		
		
		champs.add(login);
		champs.add(tLogin);
		champs.add(Box.createGlue());

		
		
		champs.add(mdp);
		champs.add(tMdp);
		champs.add(Box.createGlue());
	

		champs.setBorder(titre);
		
		this.add(champs,BorderLayout.CENTER);
		
		// Ajout espace vide à gauche
		
		FlowLayout fenetreVideE = new FlowLayout();
		espaceE.setLayout(fenetreVideE);
		espaceE.add(espaceVide1);
		this.add(espaceE,BorderLayout.EAST);
		
		// Ajout espace vide à droite
		
		FlowLayout fenetreVideW = new FlowLayout();
		espaceW.setLayout(fenetreVideW);
		espaceW.add(espaceVide2);
		this.add(espaceW,BorderLayout.WEST);
		
		// Ajout du bouton valider
		
		FlowLayout fenetreValider = new FlowLayout(FlowLayout.RIGHT);
		
		boutonVal.setLayout(fenetreValider);
		
		boutonVal.add(valider);
		boutonVal.add(espaceVide);
		this.add(boutonVal,BorderLayout.SOUTH);
		
		// Ajout des ecouteurs sur les champs et le bouton
		
		valider.addActionListener(this);
		tNom.addInputMethodListener(this);
		tPrenom.addInputMethodListener(this);
		tMail.addInputMethodListener(this);
		tNumTel.addInputMethodListener(this);
		tLogin.addInputMethodListener(this);
		tMdp.addInputMethodListener(this);
		
	}
	

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String nom = tNom.getText();
		String prenom = tPrenom.getText();
		String mail = tMail.getText();
		String tel = tNumTel.getText();
		String login = tLogin.getText();
		String mdp = tMdp.getText();
		Controle controleChampsCreation = new Controle();
		//Confirmation frameCompteValide = new Confirmation();
		if (controleChampsCreation.checkChamps(nom,prenom,mail,tel,login,mdp))
			{GestionMessagerie gestionChampsCreation = new GestionMessagerie();
			gestionChampsCreation.enregistrerChamps(nom,prenom,mail,tel,login,mdp);
			//frameCompteValide.activerFrame();
			this.desactiverFrame();
			}
		else {
			// Renvoyer a la frame de champs manquants
		}
		
	}


	@Override
	public void windowActivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowClosed(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowClosing(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowDeactivated(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowDeiconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowIconified(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void windowOpened(WindowEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void caretPositionChanged(InputMethodEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void inputMethodTextChanged(InputMethodEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	public void activerFrame() {
		this.setVisible(true);
	}
	
	public void desactiverFrame() {
		this.setVisible(false);
	}

	 
}
