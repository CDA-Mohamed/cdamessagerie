package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class ErreurConnexion extends JFrame implements ActionListener, TextListener, WindowListener {

	private JButton buttonCreacompte = new JButton("Création de compte");
	private JButton buttonConnexion = new JButton("Connexion");
	private JLabel cda20156 = new JLabel("CDA 20156 - Messagerie");
	private JLabel login = new JLabel("Login");
	private JLabel motDePasse = new JLabel("Mot de passe");
	private JTextField champLogin = new JTextField();	
	private JTextField champMotDePasse = new JTextField();
	private JLabel identifiantKO = new JLabel("Identifiant/mot de passe incorrects");
	
	
	//Création de la fenêtre de connexion afin que l'utilisateur puisse accéder à son compte.
	
	public ErreurConnexion () {
		super();
		
		// Ajout d'une icone personnalisée
		
		Toolkit kit = Toolkit.getDefaultToolkit();		
		Image img = kit.getImage("C:/ENV/messenger.png");
		setIconImage(img);
		
		// Ici,je construis ma frame principale
		
		this.setTitle("CDA Messagerie");
		BorderLayout Frame = new BorderLayout();
		this.setLayout(Frame);
		this.setSize(500, 200);
		this.setBounds(600,200, 750, 200);
	
		
		Panel p1 = new Panel();
		GridLayout gl = new GridLayout(2,3,10,10);
		p1.setLayout(gl);
		p1.add(Box.createGlue());
		p1.add(cda20156);
		
		p1.add(Box.createGlue());
		p1.add(Box.createGlue());
		p1.add(identifiantKO);
		p1.add(Box.createGlue());
		Font font = new Font("Verdana",Font.BOLD, 12);
		identifiantKO.setFont(font);
		cda20156.setFont(font);
		identifiantKO.setForeground(Color.RED);
		this.add(p1,BorderLayout.NORTH);
		
		Panel p2 = new Panel();
		GridLayout grid = new GridLayout(2,4,10,10);
		p2.setLayout(grid);
		
		p2.add(Box.createGlue());
		p2.add(login);
		p2.add(champLogin);
		p2.add(Box.createGlue());
		p2.add(Box.createGlue());
		p2.add(motDePasse);
		p2.add(champMotDePasse);
		p2.add(Box.createGlue());
		
		this.add(p2,BorderLayout.CENTER);
		
		Panel p3 = new Panel();
		FlowLayout flow = new FlowLayout(FlowLayout.CENTER);
		p3.setLayout(flow);
		//p3.setPreferredSize(new Dimension(500,100));
		p3.add(buttonCreacompte);
		p3.add(buttonConnexion);
		
		this.add(p3,BorderLayout.SOUTH);
	
		buttonCreacompte.addActionListener(this);
		buttonConnexion.addActionListener(this);
		buttonCreacompte.setName("creaCompte");
		buttonConnexion.setName("connexion");
		
		
	}
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		MenuCreationCompte mcc = new MenuCreationCompte();
		String nomBouton = ((JButton)e.getSource()).getName() ;
		
		if (nomBouton == "creaCompte") {
			mcc.activerFrame();
			this.desactiverFrame();
		}
	}
	
	public void activerFrame() {
		this.setVisible(true);
	}
	
	public void desactiverFrame() {
		this.setVisible(false);
	}

}
