package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.TextEvent;
import java.awt.event.TextListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import fr.afpa.control.Controle;
import fr.afpa.model.GestionMessagerie;
import lombok.Getter;
import lombok.Setter;
@Getter
@Setter 

public class NouveauMessage extends JFrame implements ActionListener, TextListener, WindowListener, AncestorListener {
	private JLabel nouveauMessage = new JLabel("Nouveau message");
	private JLabel a = new JLabel("À :");
	private JLabel objet = new JLabel("Objet :");
	private JTextField champa = new JTextField();	
	private JTextField champObjet = new JTextField();	
	private GridLayout grid = new GridLayout(2,3);
	private JTextArea champContenu = new JTextArea();
	private FlowLayout flowContenu = new FlowLayout();
	private JButton envoyer = new JButton("Envoyer");
	
	private JMenuBar menu = new JMenuBar();
	private JMenu fichier = new JMenu("Fichier");
	private JMenu edit = new JMenu("Édition");
	private JMenu help = new JMenu("Help");
	private JMenuItem fich = new JMenuItem("Nouveau message");
	private JMenuItem deco = new JMenuItem("Deconnexion");
	private JMenuItem edi1 = new JMenuItem("Visualiser compte");
	private JMenuItem edi2 = new JMenuItem("Modifier compte");
	private JMenuItem hel = new JMenuItem();
	
	

	
	public NouveauMessage (){
		super();
		
		this.setBounds(600,200,850,650);
		this.setTitle("CDA Messagerie");
		BorderLayout framePrincipale = new BorderLayout	();	
		this.setLayout(framePrincipale);
		
		this.setVisible(true);
		
		Panel p1 = new Panel();
		FlowLayout fl = new FlowLayout(FlowLayout.LEFT);
		p1.setLayout(fl);
		p1.add(nouveauMessage);
		Font font = new Font ("Verdana",Font.BOLD, 14);
		nouveauMessage.setFont(font);
		this.add(p1,BorderLayout.NORTH);
		
		Panel p2 = new Panel();
		p2.setLayout(grid);
	
		p2.add(a);
		p2.add(champa);
		p2.add(Box.createGlue());
		champa.setPreferredSize(new Dimension(500,30));
		p2.add(objet);
		p2.add(champObjet);
		p2.add(Box.createGlue());
		champObjet.setPreferredSize(new Dimension(500,30));
		
		this.add(p2,BorderLayout.CENTER);
		
		Panel p3 = new Panel();
		FlowLayout grid2 = new FlowLayout();
		p3.setLayout(grid2);
		p3.add(champContenu);
		champContenu.setPreferredSize(new Dimension(700,500));
		p3.add(envoyer);
		envoyer.setName("Envoyer");
		
		this.add(p3,BorderLayout.SOUTH);
		
		// Ajout des ecouteurs sur les champs et le bouton
		
		envoyer.addActionListener(this);
		champa.addActionListener(this);
		champObjet.addActionListener(this);
		champContenu.addAncestorListener(this);
		
		Toolkit kit = Toolkit.getDefaultToolkit();		
		Image img = kit.getImage("C:/ENV/messenger.png");
		setIconImage(img);
		
		// Ajout de la barre de menu
		
		fichier.add(fich);
		fichier.add(deco);
		edit.add(edi1);
		edit.add(edi2);
		help.add(hel);
		menu.add(fichier); 
		menu.add(edit);
		menu.add(help);
		this.setJMenuBar(menu);
		
		
	}
	

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
System.exit(0);
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void textValueChanged(TextEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		

			Controle controleEnvoiMess = new Controle();
			if (controleEnvoiMess.checkChampEnvoi(champa.getText(), champObjet.getText(),champContenu.getText())) {
			GestionMessagerie gestionChampEnvoi = new GestionMessagerie();
			gestionChampEnvoi.envoyerMessage(champa.getText(), champObjet.getText(),champContenu.getText());
			}
		}
		
		// TODO Auto-generated method stub




	@Override
	public void ancestorAdded(AncestorEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void ancestorMoved(AncestorEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void ancestorRemoved(AncestorEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	public void activerFrame() {
		this.setVisible(true);
	}
	
	public void desactiverFrame() {
		this.setVisible(false);
	}

}
