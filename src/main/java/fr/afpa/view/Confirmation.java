package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Label;
import java.awt.Panel;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.Border;
public class Confirmation extends JFrame implements ActionListener {
	
	
	JLabel compte = new JLabel("                                  ");
	Panel p = new Panel(new FlowLayout(FlowLayout.LEFT));
	JLabel confirm = new JLabel("Votre compte a été crée avec succes,");
	JButton b = new JButton("Retour à la page d'authenfication >");
	JPanel pcenter = new JPanel(new FlowLayout(FlowLayout.CENTER));
	JPanel pnorth = new JPanel();
	JPanel peast = new JPanel();
	JPanel pwest = new JPanel();
	JPanel pc = new JPanel();
	Border blackline = BorderFactory.createTitledBorder(" Création de compte");
	JPanel psouth = new JPanel();
	private Label t1 = new Label("                                 ");
	private Label t3 = new Label("                                 ");
	private Label t2 = new Label("             ");

	public Confirmation() {
		super();
	
		pnorth.setLayout(new BorderLayout());
		this.setTitle("CDA Messagerie");
		p.add(compte);
		pc.add(confirm);
		pnorth.add(p,BorderLayout.WEST);
		pc.add(b);
		this.add(pnorth,BorderLayout.NORTH);
		pcenter.add(pc);
		//pcenter.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.black));
		pcenter.setBorder(blackline);
		this.add(pcenter,BorderLayout.CENTER);
		
		pwest.add(t3);
		psouth.add(t1);
		peast.add(t2);
		this.add(peast,BorderLayout.EAST);
		this.add(pwest,BorderLayout.WEST);
		this.add(psouth,BorderLayout.SOUTH);
		
		b.addActionListener(this);
	}
	
	public void confirmDisplay() {
		
		this.pack();
    	this.setVisible(true);
	}
	
	public void desactiverFrame() {
    	this.setVisible(false);
	}
	
	

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Connexion nm = new Connexion();
		nm.activerFrame();
		this.desactiverFrame();
	}
	
	
}
