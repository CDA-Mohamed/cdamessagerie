package fr.afpa.view;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Mail {
	
	private String expediteur = "Ikea Lille";
	private String objet = "Ce sera dur de quitter l'été";
	private String message = "Dans dzpfk,rpe,fppv";

	public Mail() {
		super();
	} 
	
	 @Override 
	 public String toString() { 
	        return String.format(expediteur+ "      "+ objet + "    "+ message); 
	    } 

}
