package fr.afpa.view;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputMethodEvent;
import java.awt.event.InputMethodListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;

import fr.afpa.beans.Message;
import fr.afpa.beans.Utilisateur;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor

public class MenuContenuMessage extends JFrame implements ActionListener, InputMethodListener, WindowListener {
	
	
	
	private JLabel videGauche = new JLabel("          ");
	private JLabel videDroit = new JLabel("                                    ");
	private JPanel espaceGauche = new JPanel();
	private JPanel espaceDroit = new JPanel();
	private JPanel espaceBas = new JPanel();
	private JPanel panelNord = new JPanel();
	private JPanel panelCentre = new JPanel();
	private JPanel panelMessage = new JPanel();
	private String user;
	private String expediter;
	private String nom;
	private String prenom;
	private String objetm = "lol";
	private String contenu;
	private JLabel bienvenue = new JLabel("Bonjour "+ nom + " " + prenom);
	private JLabel expediteur = new JLabel("Envoyé par : "+ expediter);
	private JLabel destinataire = new JLabel("A : "+ user);
	private JLabel objet = new JLabel("Objet : "+ objetm);
	private Border cadre = BorderFactory.createTitledBorder("Contenu :");
	private JTextArea message = new JTextArea("");
	private JMenuBar menu = new JMenuBar();
	private JMenu fichier = new JMenu("Fichier");
	private JMenu edit = new JMenu("Édition");
	private JMenu help = new JMenu("Help");
	private JMenuItem fich = new JMenuItem("Nouveau message");
	private JMenuItem deco = new JMenuItem("Deconnexion");
	private JMenuItem edi1 = new JMenuItem("Visualiser compte");
	private JMenuItem edi2 = new JMenuItem("Modifier compte");
	private JMenuItem hel = new JMenuItem();
	
	public MenuContenuMessage(Message m) {
		

		this.setUser(m.getBenificiare());
		this.setExpediter(m.getExpediteur());
		this.setContenu(m.getContenu());
		this.setObjetm(m.getObjet());
		
		//this.setBienvenue(new JLabel("Bonjour "+ m.getUser().getNom() + " " + m.getUser().getPrenom()));
		this.setDestinataire( new JLabel("A : "+ m.getBeneficiaire()));
		this.setExpediteur(new JLabel("Envoyé par : "+ m.getExpediteur()));
		this.setMessage(new JTextArea(m.getContenu()));
		this.setObjet(new JLabel("Objet : "+ objetm));
		
		this.setTitle("CDA Messagerie");
		BorderLayout borderFrame = new BorderLayout();
		this.setLayout(borderFrame);
		this.setBounds(600,200, 700, 600);
		this.setResizable(false);		
		this.addWindowListener(this);
		
		// Ajout d'une icone personnalisée
		
				Toolkit kit = Toolkit.getDefaultToolkit();		
				Image img = kit.getImage("C:/ENV/messenger.png");
				setIconImage(img);
				
		
		FlowLayout fenetreTitre = new FlowLayout(FlowLayout.LEFT);
		panelNord.setLayout(fenetreTitre);
		Font font = new Font("Verdana", Font.BOLD, 20);
		Font font2 = new Font ("Verdana",Font.BOLD, 14);
		bienvenue.setFont(font);
		panelNord.add(bienvenue);
		this.add(panelNord,BorderLayout.NORTH);
		
		GridLayout grilleMessage = new GridLayout(3, 2);
		panelCentre.setLayout(grilleMessage);
		expediteur.setFont(font2);
		panelCentre.add(expediteur);
		panelCentre.add(Box.createGlue());
	
		destinataire.setFont(font2);
		panelCentre.add(destinataire);
		panelCentre.add(Box.createGlue());

		objet.setFont(font2);
		panelCentre.add(objet);
		panelCentre.add(Box.createGlue());
	
		this.add(panelCentre, BorderLayout.CENTER);
		
		BorderLayout fenetreSud = new BorderLayout();
		panelMessage.setBorder(cadre);
		panelMessage.setLayout(fenetreSud);
		message.setSize(500, 400);
		message.setPreferredSize(message.getSize());
		panelMessage.add(message,BorderLayout.NORTH);
		panelMessage.add(espaceBas,BorderLayout.CENTER);
		
		this.add(panelMessage,BorderLayout.SOUTH);
		
		FlowLayout flowW = new FlowLayout();
		espaceGauche.setLayout(flowW);
		espaceGauche.add(videGauche);
		this.add(espaceGauche,BorderLayout.WEST);
		
		FlowLayout flowE = new FlowLayout();
		espaceDroit.setLayout(flowE);
		espaceDroit.add(videDroit);
		this.add(espaceDroit,BorderLayout.EAST);
		message.setEditable(false);
		
		// Ajout de la barre de menu
		
		fichier.add(fich);
		fichier.add(deco);
		edit.add(edi1);
		edit.add(edi2);
		help.add(hel);
		menu.add(fichier); 
		menu.add(edit);
		menu.add(help);
		
		
		
		this.setVisible(true);
		
	}

	@Override
	public void windowActivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosed(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowClosing(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowIconified(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void windowOpened(WindowEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void caretPositionChanged(InputMethodEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void inputMethodTextChanged(InputMethodEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	
	
	public void desactiverFrame() {
		this.setVisible(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	

}
