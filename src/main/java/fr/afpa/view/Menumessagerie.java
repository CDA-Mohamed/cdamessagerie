package fr.afpa.view;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.border.Border;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import fr.afpa.beans.Message;

public class Menumessagerie  extends JFrame  {
	
	//Créer le conteneur des onglets
	JTabbedPane onglets = new JTabbedPane();
	JList<Message> l = new JList<>();
	DefaultListModel<Message> model = new DefaultListModel();
	JPanel pcenter = new JPanel(new FlowLayout(FlowLayout.CENTER));
	JPanel pnorth = new JPanel();
	JPanel peast = new JPanel();
	JPanel pwest = new JPanel();
	JPanel p1 = new JPanel();
	JPanel p2 = new JPanel();
	JPanel p3 = new JPanel();
	JMenuBar menu = new JMenuBar();
	JPanel psouth = new JPanel();
	JMenu fichier = new JMenu("Fichier");
	JMenu edit = new JMenu("Édition");
	JMenu help = new JMenu("Help");
	JMenuItem fich = new JMenuItem("Nouveau message");
	JMenuItem edi1 = new JMenuItem("Visualiser compte");
	JMenuItem edi2 = new JMenuItem("Modifier compte");
	JMenuItem hel = new JMenuItem();
	JMenuItem mess = new JMenuItem("Deconnexion");
	JPanel pn1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
	private Label t1 = new Label("                                 ");
	private Label t3 = new Label("                                 ");
	private Label t2 = new Label("             ");
	String expediteur = "ikea lille";
	
	NouveauMessage nm = new NouveauMessage();
	
	public Menumessagerie() throws HeadlessException {
		super();
		this.setTitle("CDA Messagerie");
		//pn1.setLayout(new GridLayout(2, 2));
		fich.addActionListener(new ActionListener() {
	    	
			
			@Override
            public void actionPerformed(ActionEvent e) {
               nm.activerFrame();

			}
		
		});
		fichier.add(fich);
		fichier.add(mess);
		edit.add(edi1);
		edit.add(edi2);
		help.add(hel);
		menu.add(fichier); 
		menu.add(edit);
		menu.add(help);
		pn1.add(menu);
		//pnorth.add(pn1);
 		p1.setLayout(new FlowLayout());
		
		l.setModel(model);
		model.addElement(new Message());
		model.addElement(new Message());
		l.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				 l.getSelectedValue();
				 
				 MenuContenuMessage mcm = new MenuContenuMessage(l.getSelectedValue());
				 //desactiverFrame();
			}
		});
		p1.add(l);
		
		BorderLayout borderFrame = new BorderLayout();
		this.setJMenuBar(menu);
		this.setLayout(borderFrame);
		this.setBounds(600,200, 700, 350);
		//onglets.setBounds(40,20,300,300);
		onglets.add("Boite de reception", p1);
	    onglets.add("Messages envoyés", p2);
	    onglets.add("Recherche de mail", p3);
	    
	    
	    pwest.add(t3);
		psouth.add(t1);
		peast.add(t2);
	    this.add(pnorth,BorderLayout.NORTH);
	    this.add(peast,BorderLayout.EAST);
		this.add(pwest,BorderLayout.WEST);
		this.add(psouth,BorderLayout.SOUTH);
	    this.add(onglets);
	    //this.setBounds(600,200, 700, 350);
	    
	    
	}
	
	public void activerFrame() {
		this.setVisible(true);
	}
	
	public void desactiverFrame() {
		this.setVisible(false);
	}
}

	