package fr.afpa.dao;



import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import fr.afpa.beans.Message;
import fr.afpa.beans.Utilisateur;
import fr.afpa.session.HibernateUtils;
import fr.afpa.view.Confirmation;
import fr.afpa.view.ConfirmationMess;
import fr.afpa.view.Menumessagerie;

public class GestionDao {

	public void enregistrerUser(Utilisateur user) {
		
		try {
			Session session = HibernateUtils.currentSession();				   
			Transaction tx = session.beginTransaction();		
			session.save(user);
			tx.commit();
			HibernateUtils.closeSession();
			
			Confirmation frameCompteValide = new Confirmation();
			frameCompteValide.confirmDisplay();
			
		} catch (Exception e) {
		
	}

	}
/**
 * Fonction permettant de recuperer une liste de messages grace au bouton Rechercher
 * @param message : La fonction prend en parametre un message contenant le mot-clé recherché
 */
	

	public List<Message> rechercherMessage(String motcle) {
		List <Message> listeMessages = new ArrayList<>();
		try {
			Session session = HibernateUtils.currentSession();				   
			Query query = session.getNamedQuery("rechercheMessage");
	    	query.setParameter("motcle", motcle);
	    	listeMessages = (ArrayList<Message>)query.getResultList();
			
			
		} catch (Exception e) {
		
	}
		return listeMessages;
	}
/**
 * Fonction permettant d'envoyer un message
 * @param message : la fonction prend en paramètre un message pour l'envoyer au destinaire
 */
	

	public void envoyerMessage(Message message) {
		try {
			Session session = HibernateUtils.currentSession();				   
			Transaction tx = session.beginTransaction();					
			session.save(message);
			tx.commit();
			HibernateUtils.closeSession();
			
			ConfirmationMess frameConfMess = new ConfirmationMess();
			frameConfMess.activerFrame();
			
		} catch (Exception e) {
		
	}
		
	}
	
	public Utilisateur checkAuthentification (Utilisateur user) {
	      try {
	        Session session = HibernateUtils.currentSession();  
	        Query query = session.getNamedQuery("RechercheLoginEtMotDePasse");
	        query.setParameter("login", user.getLogin());
	        query.setParameter("mdp", user.getMdp());
	        System.out.println(query.getSingleResult());
	        ArrayList<Utilisateur>listeU = (ArrayList<Utilisateur>)query.getResultList();
	        if( listeU!= null && listeU.size()==1) {
	      	  
	        	return listeU.get(0);
	         	
	         	
	        }
	      	} catch (Exception e) {
	      }
	      return null;
	}

}

